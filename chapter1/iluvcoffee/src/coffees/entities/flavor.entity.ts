import { Column, Entity, ManyToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Coffee } from './coffee.entity';

@Entity('flavor')
export class Flavor {

    @Column('integer', { nullable: false })
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { nullable: false })
    name: string;

    @ManyToMany(
        type => Coffee,
        coffee => coffee.flavors
    )
    coffees: Coffee[];
}
