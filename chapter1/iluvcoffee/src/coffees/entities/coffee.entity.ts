import { Column, Entity, JoinTable, ManyToMany, PrimaryGeneratedColumn } from "typeorm";
import { Flavor } from "./flavor.entity";

@Entity('coffee')
export class Coffee {

    @Column('integer', { nullable: false })
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar', { nullable: false })
    name: string;

    @Column({ nullable: true })
    description: string;

    @Column('varchar', { nullable: false })
    brand: string;

    @Column('integer', { default: 0 })
    recommendations: number;

    @JoinTable()
    @ManyToMany(
        type => Flavor,
        (flavor) => flavor.coffees,
        { cascade: true }
    )
    flavors: Flavor[];
}