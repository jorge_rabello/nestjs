import { Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Index(['name', 'type'])
@Entity()
export class Event {

    @Column('integer')
    @PrimaryGeneratedColumn()
    id: number;

    @Column('varchar')
    type: string;

    @Index()
    @Column('varchar')
    name: string;

    @Column('json')
    payload: Record<string, any>;
}
